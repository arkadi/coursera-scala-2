package funsets

object Main extends App {
  import FunSets._

  val s = singletonSet(1)
  println("contains " + contains(s, 1))
  
  val u = union(s, singletonSet(2))
  println(s"union ${contains(u, 1)} ${contains(u, 3)}")
  
  val i = intersect(singletonSet(1), u)
  println(s"intersects ${contains(i, 1)} ${contains(i, 2)}")
  
  val d = diff(u, singletonSet(1))
  println(s"diff ${contains(d, 1)} ${contains(d, 2)}")
  
  val f = filter(u, (what: Int) => what == 1)
  println(s"filter ${contains(f, 1)} ${contains(f, 2)}")

  println("forall " + forall(u, x => x == 1 || x == 2))

  println("exists " + exists(u, _ == 3))
}
